'use strict'

import React, { Component } from 'react';
import { Router, Scene } from 'react-native-router-flux';

import AppContainer from './containers/core/App'

class Routes extends Component {
  render() {
    return (
      <Router>
        <Scene
          key="app"
          component={AppContainer}
          title="App" />
      </Router>
    )
  }
};

export default Routes;
