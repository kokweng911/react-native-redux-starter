const route = (state = {}, action) => {
  switch (action.type) {
    case 'GOTO_SCENE':
      return Object.assign({}, state, {
        scene: action.scene,
        params: action.params
      });
    default:
      return state;
  }
};

export default route;
