import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'remote-redux-devtools';
import thunkMiddleware from 'redux-thunk';

import reducers from './reducers';

const composeEnhancers = composeWithDevTools({ realtime: true });
const store = createStore(reducers,
  composeEnhancers(
    applyMiddleware(
      thunkMiddleware
    )
  )
);

export default store;
